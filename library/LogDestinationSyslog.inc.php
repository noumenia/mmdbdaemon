<?php
/**
 * Log destination to syslog implementation class
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package log
 * @subpackage log
 */
class LogDestinationSyslog implements LogDestinationInterface {

	/**
	 * Syslog prefix
	 * @var string
	 */
	private $prefix = "";

	/**
	 * Syslog facility
	 * @var int
	 */
	private $facility = LOG_DAEMON;

	/**
	 * Constructor.
	 * Accepts the following parameters:
	 * - 'prefix' Prefix name added to each message
	 * - 'facility' Syslog facility
	 * @param array<string, int|string> $parameters Various parameters relevant to the Destination
	 * @return void
	 */
	public function __construct($parameters = array())
	{

		// Set the prefix based on parameters or the script filename
		if(
			isset($parameters['prefix']) &&
			is_string($parameters['prefix'])
		)
			$this->prefix = $parameters['prefix'];
		elseif(
			isset($_SERVER['PHP_SELF']) &&
			!empty($_SERVER['PHP_SELF']) &&
			is_scalar($_SERVER['PHP_SELF'])
		)
			$this->prefix = basename(strval($_SERVER['PHP_SELF']));
		else
			$this->prefix = "daemon";

		// Set the facility
		if(
			isset($parameters['facility']) &&
			is_int($parameters['facility'])
		)
			$this->facility = $parameters['facility'];

	}

	/**
	 * Write string to syslog
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array())
	{

		// Open syslog with a prefix, supply the PID along with the message
		openlog($this->prefix, LOG_CONS | LOG_PERROR | LOG_PID, $this->facility);

		// Generate a system log message
		syslog($priorityLevel, $string);

	}

}

