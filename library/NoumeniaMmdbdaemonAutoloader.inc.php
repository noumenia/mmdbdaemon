<?php
/**
 * mmDbDaemon - AutoLoader
 *
 * PHP autoload class functionality
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage autoloader
 */

/**
 * Autoload function for file classes
 * @param string $className Class name as requested by PHP
 * @return void
 */
function NoumeniaMmdbdaemonAutoloader($className)
{

	// Add extension
	$classFile = $className . ".inc.php";

	// Load file from library
	$filePath = dirname(__DIR__) . "/library/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

	// Load file from interface
	$filePath = dirname(__DIR__) . "/interface/" . $classFile;
	if(is_file($filePath)) {

		require_once($filePath);
		return;

	}

}

