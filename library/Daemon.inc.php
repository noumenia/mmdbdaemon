<?php
/**
 * Daemon class
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage daemon
 */
class Daemon extends SocketManager {

	/**
	 * GeoIP data object
	 * @var MaxMind\Db\Reader
	 */
	private $gi;

	/**
	 * Constructor
	 * @param string $effectiveUser Effective process user owner
	 * @param string $effectiveGroup Effective process group owner
	 * @param string $filePid PID file
	 * @param int $processLimit Limit the number of forked processes
	 * @param string $connection Socket file or IPv4/IPv6 address/port string ("unix|socket:/path/to/file.sock" or "inet:port@IP")
	 * @param array<string, bool|int|string> $ssl SSL/TLS context options (@see https://www.php.net/manual/en/context.ssl.php)
	 * @param string $mmdb MMDB file (eg: GeoLite2-City.mmdb)
	 * @throws InvalidArgumentException
	 * @throws MaxMind\Db\Reader\InvalidDatabaseException
	 * @return void
	 */
	public function __construct(
		string $effectiveUser = "",
		string $effectiveGroup = "",
		string $filePid = "",
		int $processLimit = 1024,
		string $connection = "",
		array $ssl = array(),
		string $mmdb = "/usr/share/GeoIP/GeoLite2-City.mmdb"
	) {

		// MaxMind\Db\Reader
		$this->gi = new MaxMind\Db\Reader($mmdb);

		// Call the socket manager constructor
		parent::__construct($effectiveUser, $effectiveGroup, $filePid, $processLimit, $connection, $ssl);

	}

	/**
	 * Process as a child, called when the parent process forks
	 * @return bool
	 */
	public function processChild(): bool
	{

		Log::debug("[mmDbDaemon: process forked child process]");

		// Read data from the client
		$clientData = $this->socketReadByNul();

		// Ignore invalid connections or empty data
		if(empty($clientData))
			return true;

		try {

			// Get DB information
			/** @var array<mixed> $rc */
			$rc = $this->gi->get($clientData);

		} catch(\Exception $e) {

			// Catch errors and return false
			$rc = false;

		}

		// Return DB information to the client
		$this->socketWriteByNul(json_encode($rc) . "\0");

		return true;

	}

}

