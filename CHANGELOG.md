# Changelog mmDbDaemon

## Development

- Remove Error catch that is never used
- Store and compare PID as an integer
- Improvements for strict type checking
- Optimize socket read functions
- Set maxMessageSize as a protected variable

## Release 1.8

- Report PID errors via Log::error
- Create the PID directory before dropping privileges

## Release 1.7

- Properly define the runtime /run/mmDbDaemon directory
- Create the PID directory if it does not already exist

## Release 1.6

- Add SSL/TLS option
- Match the constructor of the upstream SocketManager

## Release 1.5

- Strict type checking
- Ignore warnings on garbage input
- Get the status of the child process that just ended
- Set default socket directory as /run/mmDbDaemon
- Improve the loading of the help controller
- Do not enforce parameters
- Add markdown to the ASCII logo
- Set default PID directory as /run/mmDbDaemon
- Preserve the runtime directory via systemd

## Release 1.4

- Implement the Log facility parameter for syslog

## Release 1.3

- Correct effective user setting
- Enable socket reuse address mode
- Disable slow debug messages
- Improve socket read size calculation
- Lower case executable

## Release 1.2

- Composer integration and README.md instructions
- Support for library files under /usr/share/php
- Source prefix based on PHP_SELF
- Handle exceptions from the MaxMind DB Reader
- Prepare RPM packages

## Release 1.1

- Harmonize Log across all projects
- Documentation updates for the SocketManager

## Release 1.0

- Initial public release

