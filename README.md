# mmDbDaemon

```
                      ____  _     ____
  _ __ ___  _ __ ___ |  _ \| |__ |  _ \  __ _  ___ _ __ ___   ___  _ __
 | '_ ` _ \| '_ ` _ \| | | | '_ \| | | |/ _` |/ _ \ '_ ` _ \ / _ \| '_ \
 | | | | | | | | | | | |_| | |_) | |_| | (_| |  __/ | | | | | (_) | | | |
 |_| |_| |_|_| |_| |_|____/|_.__/|____/ \__,_|\___|_| |_| |_|\___/|_| |_|

```

mmDbDaemon is a memory-resident MaxMind Database reader implementation in PHP.

Instead of loading the MaxMind database over and over again for multiple queries, mmDbDaemon offers a simple way to run queries on a persistent memory-resident object that forks separate processes in parallel and re-using the same database.

# Features

- JSON reply
- Listen on IP address/port or UNIX sockets
- Support for signals


# Requirements

- [MaxMind-DB-Reader-php](https://github.com/maxmind/MaxMind-DB-Reader-php)
- PHP 7.2, 7.3, 7.4, 8.0, 8.1, 8.2, 8.3
- iconv module
- posix module
- sockets module


# Install with RPM packages

You may install mmDbDaemon via the copr repository, for Alma/Rocky/Oracle Enterprise Linux and Fedora, simply use:

```
dnf copr enable mksanthi/noumenia
dnf install mmDbDaemon
```


# Install with Composer

You may install mmDbDaemon with composer, to get the latest version use the create-project command, the last dot is important because it tells composer to save the files in the current directory.

```
composer create-project noumenia/mmdbdaemon .
```


# How to use

Once the daemon is running, connect to the socket or IP/port and send the IP address terminated by a NULL. The daemon will respond with a JSON-formatted database result.

```
Usage: mmdbdaemon [OPTION]...

  -V,  --version             display version information only
  -h,  --help                display help about parameters
  -v,  --verbose             enable verbose output to stdout

  -u,  --user                Effective user
  -g,  --group               Effective group
  -p,  --pid                 PID file
  -l,  --processlimit        Process limit
  -c,  --connection          Connection string
  -a,  --autoload            MaxMind DB Reader PHP autoload.php file
  -m,  --mmdb                MMDB file (eg: GeoLite2-City.mmdb)

  The connection string can be a UNIX socket or an IPv4/IPv6 address/port.
  UNIX Socket  : unix:/run/mmDbDaemon/mmDbDaemon.sock
  Address/port : inet:9999@127.0.0.1

```


# Example client

```php
<?php

// Connect to a socket and open a stream
$fp = @stream_socket_client("unix:///run/mmDbDaemon/mmDbDaemon.sock", $errno, $errstr);

// or to connect via TCP
// $fp = stream_socket_client("tcp://127.0.0.1:9999", $errno, $errstr);

// IP address terminated by a NULL character
fwrite($fp, "46.101.128.168\0");

// Loop
while(!feof($fp)) {

	// Read from the stream
	echo fgets($fp, 1024);

}

// Close the stream
fclose($fp);

```

# Related projects

- [Aetolos -  Virtual hosting at your... command line!](https://gitlab.com/noumenia/aetolos)
- [libMilterPHP is a Postfix/Sendmail Milter library implementation in PHP.](https://gitlab.com/noumenia/libmilterphp)
- [guardian-milter - Multi-purpose security milter.](https://gitlab.com/noumenia/guardian-milter)

