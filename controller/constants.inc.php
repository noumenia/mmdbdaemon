<?php
/**
 * mmDbDaemon - Constants
 *
 * Define system-wide constants
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage constants
 */

// Set application version
define("DAE_VER", "1.9-dev");

