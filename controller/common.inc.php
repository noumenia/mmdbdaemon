<?php
/**
 * mmDbDaemon - Common
 *
 * Common functionality
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage common
 */

// Initialize constants
require_once(dirname(__DIR__) . "/controller/constants.inc.php");

// Initialize the autoloader
require_once(dirname(__DIR__) . "/library/NoumeniaMmdbdaemonAutoloader.inc.php");

// Register the autoloader
spl_autoload_register("NoumeniaMmdbdaemonAutoloader", true);

// Initialize logging
require_once(dirname(__DIR__) . "/controller/loginit.inc.php");

