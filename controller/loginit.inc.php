<?php
/**
 * mmDbDaemon - Initialize log reporting
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage log
 */

// Set destination of log messages
if(
	isset($cmdParameters['v']) ||
	isset($cmdParameters['verbose'])
)

	// Verbose output to syslog
	Log::setDestination(new LogDestinationSyslog(), LOG_DEBUG);

else

	// Display warnings or higher priority messages to syslog
	Log::setDestination(new LogDestinationSyslog(), LOG_WARNING);

