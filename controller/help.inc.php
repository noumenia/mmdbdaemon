<?php
/**
 * mmDbDaemon - Help
 *
 * Display help for command-line parameters and arguments
 *
 * @copyright Noumenia (C) 2023 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package mmDbDaemon
 * @subpackage help
 */

// No direct access - loadable only
if(!defined("DAE_IN"))
	die("No Access");

// Display logo
echo chr(27) . "[34;1mmmDbDaemon" . chr(27) . "[0m v" . DAE_VER . " Copyright Noumenia 2023, GNU GPL v3.0\n";

// Premature exit for version only
if(
	isset($cmdParameters['V']) ||
	isset($cmdParameters['version'])
)
	exit();

echo "\nUsage: mmdbdaemon [OPTION]...\n\n";

// Array of help parameters
$help = array(
	array('short' => "V",	'long' => "version",		'desc' => "display version information only"),
	array('short' => "h",	'long' => "help",		'desc' => "display help about parameters"),
	array('short' => "v",	'long' => "verbose",		'desc' => "enable verbose output to stdout"),
	"",
	array('short' => "u",	'long' => "user",		'desc' => "Effective user"),
	array('short' => "g",	'long' => "group",		'desc' => "Effective group"),
	array('short' => "p",	'long' => "pid",		'desc' => "PID file"),
	array('short' => "l",	'long' => "processlimit",	'desc' => "Process limit"),
	array('short' => "c",	'long' => "connection",		'desc' => "Connection string"),
	array('short' => "a",	'long' => "autoload",		'desc' => "MaxMind DB Reader PHP autoload.php file"),
	array('short' => "m",	'long' => "mmdb",		'desc' => "MMDB file (eg: GeoLite2-City.mmdb)"),
	"",
	" The connection string can be a UNIX socket or an IPv4/IPv6 address/port.",
	" UNIX Socket...: unix:/run/mmDbDaemon/mmDbDaemon.sock",
	" Address/port..: inet:9999@127.0.0.1"
);

// Display help
foreach($help as $h) {

	if(is_string($h))
		echo $h . "\n";
	else
		echo "  -" . $h['short'] . ",  --" . str_pad($h['long'], 20, " ", STR_PAD_RIGHT) . $h['desc'] . "\n";

}

exit();

