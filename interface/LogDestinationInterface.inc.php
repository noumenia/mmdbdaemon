<?php
/**
 * Log destination interface
 *
 * @copyright Noumenia (C) 2019 - All rights reserved - Software Development - www.noumenia.gr
 * @license GNU GPL v3.0
 * @package log
 * @subpackage log
 */
interface LogDestinationInterface {

	/**
	 * Constructor.
	 * @param array<string, int|string> $parameters Various parameters relevant to the Destination
	 * @return void
	 */
	public function __construct($parameters = array());

	/**
	 * Write string to output
	 * @param string $string Message string
	 * @param int $priorityLevel Priority level
	 * @param array<string> $parameters Extra parameters to pass to the destination writer, in the form of an array of strings
	 * @return void
	 */
	public function write($string, $priorityLevel, $parameters = array());

}

